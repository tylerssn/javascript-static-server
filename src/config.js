import path from 'path';
import fs from 'fs';
import argv from './argv';

const defaultConfig = {
  host: 'localost',
  port: 8080,
  serverRoot: process.cwd(),
};
let userConfig = {};
const commandLineConfig = {};
const projectDirectory = process.cwd();
const userConfigPath = path.resolve(projectDirectory, argv.config);
const hasUserConfig = fs.existsSync(userConfigPath);

if (hasUserConfig) {
  // eslint-disable-next-line import/no-dynamic-require,global-require
  userConfig = require(userConfigPath);
} else {
  userConfig = {};
}

if (argv.port) {
  commandLineConfig.port = argv.port;
}

if (argv.host) {
  commandLineConfig.host = argv.host;
}

export default Object.assign({}, defaultConfig, userConfig, commandLineConfig);
