import yargs from 'yargs';

yargs
  .option('p', {
    alias: 'port',
    demandOption: false,
    describe: 'bind server to a specific port. Overrides config file.',
    type: 'number',
  })
  .option('h', {
    alias: 'host',
    demandOption: false,
    describe: 'bind server to a specific host. Overrides config file.',
    type: 'string',
  })
  .option('c', {
    alias: 'config',
    default: '.serverconfigrc.js',
    demandOption: false,
    describe: 'provide a config file path relevant to the working directory.',
    type: 'string',
  })
  .help();
export default yargs.argv;
