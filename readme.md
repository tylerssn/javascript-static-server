### Summary

A non-production server designed to quickly make files within your project available via a static asset server.

# Install Dev

```bash
npm install --save-dev javascript-static-server`
```

Or 
```bash
yarn add --dev javascript-static-server`
```

# Configure

```javascript
/**
 * Copy to your projects root as .serverconfigrc.js
 *
 * @type {{port: number, host: string}}
 */
module.exports = {
  host: '127.0.0.1',
  port: 8081
};
```

# Using

`yarn static-serve --help`

`yarn static-serve --h localhost -p 80 -c .custom.config.js`
